package task10_IO_NIO.readingwriting;

/**
 * Compare reading and writing performance of usual and buffered reader
 * for 200 MB file. Compare performance of buffered reader with different buffer size
 * (e.g. 10 different size).
 */

import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.InputMismatchException;
import java.util.Scanner;
public class InputStreamTest {

    public static void main(String[] args) throws IOException {
        System.out.println("Hello User");
        System.out.println("Please enter buffer size:");

        while (true) {
            try {
                Scanner scan = new Scanner(System.in);
                int bufferSize = scan.nextInt();
                Instant start = Instant.now();
                readFile(bufferSize);
                Instant end = Instant.now();
                Duration timeElapsed = Duration.between(start, end);
                System.out.println("Time taken: " + timeElapsed.toMillis() + " milliseconds");
            }
            catch (InputMismatchException e){
                System.out.println("You have entered not a number. Please enter number");
            }
        }
    }

        private static void readFile ( int bufferSize){
               try {

                InputStream in = new FileInputStream("/home/istvan/IdeaProjects/EpamLabs/Book.pdf");
                byte[] buffer = new byte[bufferSize];
                char c;

                in.read(buffer);
                for (byte b : buffer) {
                    c = (char) b;
                }
               } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



