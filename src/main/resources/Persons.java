package task10_IO_NIO.serialized;

import java.io.*;


/**
 * Program creates abstract class Person and extends from it class
 * Worker and Student. Every object is adding to list of Persons.
 * Information about added object is printing on console.
 * Classes are serialized.
 */


public class PersonsManager {
    public static void main(String[] args) {

        Student student = new Student("Ivanov", "high");
        Worker worker = new Worker("Petrov", "teacher");
        Academy academy = new Academy();
        Academy academySerialized;
        academy.addPerson(student);
        academy.addPerson(worker);
        academy.showAll();
        try {
            FileOutputStream out = new FileOutputStream("/home/istvan/IdeaProjects/EpamLabs/src/main/resources/myFile.ser");
            ObjectOutputStream stream = new ObjectOutputStream(out);
            stream.writeObject(academy);
            stream.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileInputStream in = new FileInputStream("/home/istvan/IdeaProjects/EpamLabs/src/main/resources/myFile.ser");
            ObjectInputStream inputStream = new ObjectInputStream(in);
            academySerialized = (Academy) inputStream.readObject();
            inputStream.close();
            in.close();
        } catch (IOException a) {
            a.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Academy class is not found");
            return;
        }

        System.out.println("Before serialize :");
        for (Person person : academy.personList) {
            System.out.println(person.getName() + person.showData());
        }

        System.out.println("After serialize :");
        for (Person person : academySerialized.personList) {
            System.out.println(person.getName() + person.showData());
        }
    }
}



