package task13_XML;

public class Bank {
    private String name;
    private String country;
    private String type;

    public Bank() {
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return String.format("Bank: %s, Country: %s, Type: %s", getName(), getCountry(), getType());
    }
}
