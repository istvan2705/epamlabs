package task13_XML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLParserDOM {
    public static final String XML_PATH = "/home/istvan/IdeaProjects/EpamLabs/src/main/resources/banks.xml";

    public static void main(String[] args) {
        File xmlFile = new File(XML_PATH);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nodeList = doc.getElementsByTagName("bank");
            List<Bank> banksList = new ArrayList<>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                banksList.add(getBank(nodeList.item(i)));
            }
            banksList.forEach((s) -> System.out.println(s));
            List<Depositor> depositors = new ArrayList<>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                depositors.add(getDepositor(nodeList.item(i)));
            }
            depositors.forEach((s) -> System.out.println(s));
        } catch (SAXException | ParserConfigurationException | IOException e1) {
            e1.printStackTrace();
        }
    }

    private static Bank getBank(Node node) {
        Bank bank = new Bank();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            bank.setName(getTagValue("Name", element));
            bank.setCountry(getTagValue("Country", element));
            bank.setType(getTagValue("Type", element));
        }
        return bank;
    }

    private static Depositor getDepositor(Node node) {
        Depositor depositor = new Depositor();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            depositor.setDepositorName(getTagValue("Depositor", element));
            depositor.setAmount(Integer.parseInt(getTagValue("Amount_on_depozit", element)));
            depositor.setProfitability(Integer.parseInt(getTagValue("Profitability", element)));
            depositor.setTerm(Integer.parseInt(getTagValue("Time_constraints", element)));
        }
        return depositor;
    }

    private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }
}
