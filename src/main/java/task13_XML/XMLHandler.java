package task13_XML;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class XMLHandler extends DefaultHandler {

    boolean isName = false;
    boolean isCountry = false;
    boolean isType = false;
    boolean isDepositor = false;
    boolean isAccountID = false;
    boolean isAmountOnDeposit = false;
    boolean isProfitability = false;
    boolean isTerm = false;
    private StringBuilder data = null;
    private Bank bank = null;
    private Depositor depositor = null;
    private List<Bank> banks = null;
    private List<Depositor> depositors = null;

    public List<Bank> getBanks() {
        return banks;
    }

    public List<Depositor> getDepositors() {
        return depositors;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("bank")) {
            bank = new Bank();
            if (banks == null) {
                banks = new ArrayList<>();
            }
            depositor = new Depositor();
            if (depositors == null) {
                depositors = new ArrayList<>();
            }
        } else if (qName.equalsIgnoreCase("name")) {
            isName = true;
        } else if (qName.equalsIgnoreCase("country")) {
            isCountry = true;
        } else if (qName.equalsIgnoreCase("type")) {
            isType = true;
        } else if (qName.equalsIgnoreCase("depositor")) {
            isDepositor = true;
        } else if (qName.equalsIgnoreCase("account_ID")) {
            isAccountID = true;
        } else if (qName.equalsIgnoreCase("amount_on_depozit")) {
            isAmountOnDeposit = true;
        } else if (qName.equalsIgnoreCase("profitability")) {
            isProfitability = true;
        } else if (qName.equalsIgnoreCase("time_constraints")) {
            isTerm = true;
        }
        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (isName) {
            bank.setName(data.toString());
            isName = false;
        } else if (isCountry) {
            bank.setCountry(data.toString());
            isCountry = false;
        } else if (isType) {
            bank.setType(data.toString());
            isType = false;
        } else if (isDepositor) {
            depositor.setDepositorName(data.toString());
            isDepositor = false;
        } else if (isAccountID) {
            depositor.setAccountID(data.toString());
            isAccountID = false;
        } else if (isAmountOnDeposit) {
            depositor.setAmount(Integer.parseInt(data.toString()));
            isAmountOnDeposit = false;
        } else if (isProfitability) {
            depositor.setProfitability(Integer.parseInt(data.toString()));
            isProfitability = false;
        } else if (isTerm) {
            depositor.setTerm(Integer.parseInt(data.toString()));
            isTerm = false;
        }
        if (qName.equalsIgnoreCase("bank")) {
            banks.add(bank);
            depositors.add(depositor);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        data.append(new String(ch, start, length).trim());
    }
}

