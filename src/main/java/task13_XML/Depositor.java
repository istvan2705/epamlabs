package task13_XML;

public class Depositor {
    private String depositorName;
    private String accountID;
    private int amount;
    private int profitability;
    private int term;

    public Depositor() {
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getProfitability() {
        return profitability;
    }

    public void setProfitability(int profitability) {
        this.profitability = profitability;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public String toString() {
        return String.format("Depositor: %s, Account_ID: %s, Amount: %d, Profitability: %d, Term: %d",
                getDepositorName(), getAccountID(), getAmount(), getProfitability(), getTerm());
    }
}
