package task13_XML;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class XMLTransformToHTML {
    public static final String XSL_PATH = "/home/istvan/IdeaProjects/EpamLabs/src/main/resources/banks.xsl";
    public static final String XML_PATH = "/home/istvan/IdeaProjects/EpamLabs/src/main/resources/banks.xml";

    public static void main(String[] args) {
        try {
            TransformerFactory tFactory = TransformerFactory.newInstance();

            Source xslDoc = new StreamSource(XSL_PATH);
            Source xmlDoc = new StreamSource(XML_PATH);


            OutputStream htmlFile = new FileOutputStream("/home/istvan/IdeaProjects/EpamLabs/src/main/resources/banks.html");
            Transformer transform = tFactory.newTransformer(xslDoc);
            transform.transform(xmlDoc, new StreamResult(htmlFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}