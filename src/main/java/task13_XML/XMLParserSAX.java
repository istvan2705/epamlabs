package task13_XML;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class XMLParserSAX {

    public static final String XML_PATH = "/home/istvan/IdeaProjects/EpamLabs/src/main/resources/banks.xml";

    public static void main(String[] args) {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            XMLHandler handler = new XMLHandler();
            saxParser.parse(new File(XML_PATH), handler);
            List<Bank> banks = handler.getBanks();
            banks.forEach((s) -> System.out.println(s));
            List<Depositor> depositors = handler.getDepositors();
            depositors.forEach((s) -> System.out.println(s));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}

