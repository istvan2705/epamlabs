package task13_XML;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;


public class XMLValidator {
    public static final String XML_PATH = "/home/istvan/IdeaProjects/EpamLabs/src/main/resources/banks.xml";
    public static final String XSD_PATH = "/home/istvan/IdeaProjects/EpamLabs/src/main/resources/banks.xsd";

    public static void main(String[] args) {
        System.out.println("EmployeeRequest.xml validates against Employee.xsd? " + validateXMLSchema(XSD_PATH, XML_PATH));
    }

    public static boolean validateXMLSchema(String xsdPath, String xmlPath) {

        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(XSD_PATH));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(XML_PATH)));
        } catch (IOException | SAXException e) {
            System.out.println("Exception: " + e.getMessage());
            return false;
        }
        return true;
    }
}

