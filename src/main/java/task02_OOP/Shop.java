package task02_OOP;

import java.util.ArrayList;
import java.util.List;

public class Shop {
    private String name;
    private static ArrayList<Product> list = new ArrayList<Product>();


    public Shop(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public  ArrayList<Product> getList() {
        return list;
    }

   public void addProduct(Product product){
        list.add(product);
    }

    public ArrayList<Product> getProductBrand(String brand){
        ArrayList<Product> listOfProduct = new ArrayList<Product>();
       for (Product product: getList()){
           if (product.getBrand().equals(brand)){
               listOfProduct.add(product);
           }
       }
       return listOfProduct;

    }

    public ArrayList<Product> getProductName(String productName){
        ArrayList<Product> listOfProduct = new ArrayList<Product>();
        for (Product product: getList()){
            if(product.getProductName().equals(productName)){
                listOfProduct.add(product);
            }
        }
        return listOfProduct;
    }

    public void printProduct(List<Product> list){
       for (Product product: list) {
           System.out.println(product);
       }
    }


}
