package task02_OOP;

public class Paint extends Product {
    private String color;

    public Paint(String ID, String name, double price, String brand, double size, String color) {
        super(ID, name, price, brand, size);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
       return super.toString() +" "+ String.format("color: %s |\n",getColor());
    }
}
