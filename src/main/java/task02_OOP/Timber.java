package task02_OOP;

public class Timber extends Product {

    private String type;

    public Timber(String ID, String name, double price, String brand, double size, String type) {
        super(ID, name, price, brand, size);
        this.type = type;
    }
    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return super.toString() +" "+ String.format("type: %s |\n", getType());
    }
}

