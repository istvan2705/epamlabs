package task02_OOP;

public class Product {
    protected String ID;
    protected String productName;
    protected double price;
    protected String brand;
    protected double size;

    public Product(String ID, String name, double price, String brand, double size) {
        this.ID = ID;
        this.productName = name;
        this.price = price;
        this.brand = brand;
        this.size = size;
    }

    public String getID() {
        return ID;
    }

    public String getProductName() {
        return productName;
    }

    public double getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public double getSize() {
        return size;
    }

    @Override
    public String toString() {
        return String.format("ID: %s | productName: %s | price: %.2f | brand: %s | size: |%.2f |", getID(),
                getProductName(), getPrice(), getBrand(), getSize());
    }
}
