package task02_OOP;

import java.util.ArrayList;

public class HyperMarket {
    public static void main(String[] args) {

        Shop shop = new Shop("Arsenal");
        shop.addProduct(new Cement("1", "Cement", 145, "TP-131", 50 ));
        shop.addProduct(new Cement("2","Cement",140, "TP-131", 50));
        shop.addProduct(new Paint("23", "Paint", 141, "P-102", 25, "red"));
        shop.addProduct(new Timber("25", "Timber", 142, "Tim-21",121, "Oak"));

        ArrayList<Product> list = shop.getProductBrand("TP-131");
        ArrayList<Product> list1 = shop.getProductName("Timber");
        shop.printProduct(list);
        shop.printProduct(list1);

    }
}
