package task07_String;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {

    public static final String WORDS_SEPARATOR = "[^,:-;]";
    public static final String SENTENCES_SEPARATOR = "[.!?]";
    public static final String SPACE = " ";
    public static final String QUESTION_SENTENCES_SEPARATOR = "([А-Яа-яA-Za-z][^.!?]*)\\?";
    public static final String EMPTY_SYMBOL = "";
    public static final String PUNCTUATION_MARKS = "[\\p{Punct}«»]";
    public static final String MORE_SPACES = "\\s+";

    public static void main(String[] args) throws IOException {
        String text = getText();
        System.out.println(text);
        String[] sentences = getSentences(text);
        printSentences(sentences);
        List<String[]> sorted = getSortedListOfSentences(sentences);
        printSortedSentences(sorted);
        System.out.println(findWord(sentences));
        List<String> sent = getSentenceWithQuestionMark(text, 5);
        System.out.println(sent);
        List<String> list = getSortedTextByWord(text);
        System.out.println(list.toString());
    }

    public static String getText() throws IOException {
        String text = new String(Files.readAllBytes(Paths.get("text3.txt")), StandardCharsets.UTF_8);
        return text;
    }

    public static String[] getSentences(String input) {
        Pattern pattern = Pattern.compile(WORDS_SEPARATOR);
        Matcher matcher = pattern.matcher(input);
        StringBuilder build = new StringBuilder();
        String modifiedText = null;
        while (matcher.find()) {
            build.append(matcher.group());
            modifiedText = build.toString();
        }
        String[] arrayOfSentences = modifiedText.split(SENTENCES_SEPARATOR);
        return arrayOfSentences;
    }

    public static List<String[]> getSortedListOfSentences(String[] sentences) {
        List<String[]> list = new ArrayList<>();
        for (String sentence : sentences) {
            list.add(sentence.trim().split(SPACE));
        }
        Collections.sort(list, (x1, x2) -> {
            if (x1.length > x2.length) {
                return 1;
            }
            if (x1.length < x2.length) {
                return -1;
            }
            return 0;
        });
        return list;
    }

    public static List<String> findWord(String[] sentences) {
        String firstSentence = sentences[0];
        String[] array = firstSentence.split(SPACE);
        List<String> list = new ArrayList<>();
        for (int i = 1; i < sentences.length; i++) {
            list.add(sentences[i]);
        }
        String text = list.toString();
        System.out.println(text);
        List<String> list1 = new ArrayList<>();
        for (String word : array) {
            if (!text.contains(word)) {
                list.add(word);
            } else continue;
        }
        return list1;
    }

    public static List<String> getSentenceWithQuestionMark(String input, int length) {
        Pattern pattern = Pattern.compile(QUESTION_SENTENCES_SEPARATOR);
        Matcher matcher = pattern.matcher(input);
        String modifiedText = null;
        StringBuilder build = new StringBuilder();

        while (matcher.find()) {
            build.append(matcher.group()).append(SPACE);
            modifiedText = build.toString();
        }
        System.out.println(modifiedText);
        String[] words = modifiedText.split(MORE_SPACES);
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll(PUNCTUATION_MARKS, EMPTY_SYMBOL);
        }
        Stream<String> stream = Arrays.stream(words);

        List<String> list = stream.filter((p) -> p.length() == length).distinct()
                .collect(Collectors.toList());
        return list;
    }

    public static List<String> getSortedTextByWord(String text) {
        String[] words = text.split(MORE_SPACES);
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll(PUNCTUATION_MARKS, EMPTY_SYMBOL).toLowerCase();
        }

        Stream<String> stream = Arrays.stream(words);
        List<String> list = stream.sorted().collect(Collectors.toList());
        return list;
    }

    public static void printSentences(String[] sentences) {
        for (String s : sentences) {
            System.out.println(s);
        }
    }

    public static void printSortedSentences(List<String[]> list) {
        for (String[] array : list) {

            System.out.println(array.length);
        }
    }
}









