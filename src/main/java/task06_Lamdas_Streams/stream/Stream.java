package task06_Lamdas_Streams.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Stream {

    public static void main(String[] args) {

        List<Integer> list = randomArray(100);
        double average = list.stream().mapToDouble(i -> i).average().orElse(Double.NaN);
        int max = list.stream().max(Integer::compare).get();
        int min = list.stream().min(Integer::compare).get();
        int sum = list.stream().reduce((s1, s2) -> s1 + s2).orElse(0);
        int sum1 = list.stream().mapToInt(i -> i).sum();
        long count = list.stream().filter(p -> p > average).mapToInt(i -> i).count();

        System.out.println("List of values: ");
        System.out.println(list.toString());

        System.out.println("Average of list of values: "+average);

        System.out.print("Maximum value of list of values: ");
        printNumber(max);

        System.out.print("Minimum value of list of values: ");
        printNumber(min);

        System.out.print("Sum of values using reduce Stream: ");
        printNumber(sum);

        System.out.print("Sum of values using sum Stream: ");
        printNumber(sum1);

        System.out.print("Count number of values that are bigger than average: ");
        printNumber(count);
    }

    private static ArrayList<Integer> randomArray(int size) {
        Random random = new Random();

        ArrayList<Integer> newArrayList = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            int next = random.nextInt(256);
            newArrayList.add(next);
        }
        return newArrayList;
    }

    public static void printNumber(long a) {
        System.out.println(a);
    }
}
