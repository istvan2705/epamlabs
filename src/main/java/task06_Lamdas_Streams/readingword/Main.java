package task06_Lamdas_Streams.readingword;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class Main {
    public static void main(String[] args) {
        List<String> listOfWords = new ArrayList<>();
        int i = 0;
        String[] text = new String[10];
        Scanner sc = new Scanner(System.in);
        System.out.println("Hello User!\nPlease enter some words without punctuation marks in several lines and press" +
                " key 'enter' two times");
        while (true) {
            String word = sc.nextLine();
            if (word.equals("")) {
                break;
            }
            text[i] = word;
            listOfWords.add(text[i]);
            i++;
        }

        long amountUniqueWords = getUniqueWords(listOfWords);
        List<String> sortedWords = getSortedListWords(listOfWords);
        Map<String, Integer> frequencyWords = getFrequencyWord(listOfWords);
        Map<String, Long> frequencyChars = getFrequencyChar(listOfWords);

        System.out.print("Number of unique words: ");
        System.out.println(amountUniqueWords);
        System.out.print("Sorted list of unique words: ");
        System.out.println(sortedWords);
        System.out.print("Occurrence number of each word: ");
        printMap(frequencyWords);
        System.out.print("Occurrence number of each symbol: ");
        printMap(frequencyChars);
    }

    public static long getUniqueWords(List<String> line) {
        long amount = line.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .distinct()
                .count();

        return amount;
    }

    public static List<String> getSortedListWords(List<String> line) {

        List<String> list = line.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        return list;
    }

    public static Map<String, Integer> getFrequencyWord(List<String> line) {
        Map<String, Integer> frequencyMap = line.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(groupingBy(Function.identity(), collectingAndThen(counting(), Long::intValue)));
        return frequencyMap;
    }

    public static Map<String, Long> getFrequencyChar(List<String> line) {
        Map<String, Long> frequencyMap = line.stream()
                .flatMap(e -> Stream.of(e.split("")))
                .collect(groupingBy(a -> a, counting()));
        return frequencyMap;
    }

    public static void printMap(Map<String, ? extends Number> list) {
        System.out.println(list);

    }
}


