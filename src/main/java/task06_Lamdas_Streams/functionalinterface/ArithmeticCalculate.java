package task06_Lamdas_Streams.functionalinterface;


@FunctionalInterface
public interface ArithmeticCalculate {
    double calculate(int a, int b, int c);

}
