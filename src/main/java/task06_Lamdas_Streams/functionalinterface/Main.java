package task06_Lamdas_Streams.functionalinterface;


public class Main {

    public static void main(String[] args) {

        ArithmeticCalculate max = (a, b, c) -> {
            if (a > b && a > c) return a;
            if (b > a && b > c) return b;
            else return c;
        };
        double maximum = max.calculate(12, 6, 8);
        System.out.println("Maximum value is: " + maximum);

        ArithmeticCalculate ave = (a, b, c) -> (a + b + c) / 3;
        double average = ave.calculate(5, 8, 3);
        System.out.println("Average value is: " + average);
    }
}