package task06_Lamdas_Streams.command;

public class UpperCase implements Command {

    @Override
    public void changeString(String string) {
        String upper = string.toUpperCase();
        System.out.println(upper);
    }
}
