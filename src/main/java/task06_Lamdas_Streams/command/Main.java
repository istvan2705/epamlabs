package task06_Lamdas_Streams.command;

import java.util.Scanner;

public class Main {

    public static final String SPACE = " ";
    public static final int AMOUNT_OF_PARAMETERS = 2;
    public static final int FIRST_PARAMETER = 0;
    public static final int SECOND_PARAMETER = 1;

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        print("Hello user. Please enter your command in a format: 'Command word'. Following commands are " +
                "available\nchararray\nlength\nreverse\nuppercase\n");

        while (true) {

            String input = sc.nextLine();
            String[] word = getWords(input);

            if (word.length != AMOUNT_OF_PARAMETERS) {
                print("You have entered wrong numbers of arguments. You should enter two arguments in line");
                continue;
            }

            String command = word[FIRST_PARAMETER];
            String argument = word[SECOND_PARAMETER];
            try {
                Commands commandName = Commands.getCommand(command);

                switch (commandName) {
                    case CHARARRAY:
                        CharArray charArray = new CharArray();
                        charArray.changeString(argument);
                        break;

                    case LENGTH:
                        Length length = (a) -> a.length();
                        System.out.println(length.changeString(argument));
                        break;

                    case REVERSE:
                        new Command() {
                            @Override
                            public void changeString(String string) {
                                String reverse = new StringBuffer(string).reverse().toString();
                                print(reverse);
                            }
                        }.changeString(argument);
                        break;

                    case UPPERCASE:

                        UpperCase upperCase = new UpperCase();
                        upperCase.changeString(argument);
                        break;
                }
            } catch (IllegalArgumentException e) {
                {
                    print("Entered command is not correct. You should enter on of the following:\n" +
                            "length\nchararray\nuppercase\nreverse");
                }
            }
        }
    }

    private static void print(String s) {
        System.out.println(s);
    }

    public static String[] getWords(String input) {
        String[] words = input.split(SPACE);
        return words;
    }
}
