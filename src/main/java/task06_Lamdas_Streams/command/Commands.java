package task06_Lamdas_Streams.command;

public enum Commands {
    CHARARRAY("chararray"),
    LENGTH("length"),
    REVERSE("reverse"),
    UPPERCASE("uppercase");

    private String value;

    Commands(String value) {
        this.value = value;
    }

    public static Commands getCommand(String value) {
        if (value != null) {
            for (Commands command : Commands.values()) {
                if (value.equalsIgnoreCase(command.value)) {
                    return command;
                }
            }
        }
        throw new IllegalArgumentException();
    }
}
