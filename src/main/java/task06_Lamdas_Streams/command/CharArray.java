package task06_Lamdas_Streams.command;

public class CharArray implements Command {

    @Override
    public void changeString(String string) {
        char[] array = string.toCharArray();

        for (char a : array)
            System.out.print(a + " ");
        System.out.println();
    }
}
