package task05_Collections_part1;

public class Ship <T>  {

    public T t;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
