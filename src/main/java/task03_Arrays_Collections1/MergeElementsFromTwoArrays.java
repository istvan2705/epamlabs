package task03_Arrays_Collections1;

import java.util.Arrays;

/**
 * Дано два масиви. Сформувати третій масив, що складається з тих елементів,
 * які: а) присутні в обох масивах
 */
public class MergeElementsFromTwoArrays {
    public static void main(String[] args) {

        int[] array1 = {2, 5, 8, 25, 6, 3, 9, 1, 2, 7, 12};
        int[] array2 = {1, 4, 8, 3, 10, 12, 9, 25, 8, 6, 5};
        int size = getArraySize(array1, array2);
        int[] arrayConsistsFromArrayIndex = getIndexesFromArray(array1, array2, size);
        Arrays.sort(arrayConsistsFromArrayIndex);
        printArray(arrayConsistsFromArrayIndex);

    }

    public static int getArraySize(int[] array1, int[] array2) {
        int count = 0;
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array2[j] == array1[i]) {
                    count++;
                    break;
                }
            }
        }
        return count;
    }

    public static int[] getIndexesFromArray(int[] array1, int[] array2, int count) {
        int count1 = 0;
        int[] indexArray = new int[count];
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array2[j] == array1[i]) {
                    indexArray[count1] = array1[i];
                    count1++;
                    break;
                }
            }
        }
        return indexArray;
    }

    public static void printArray(int[] array) {
        System.out.print("Array of elements presented in both arrays: ");
        for (int j = 0; j < array.length; j++) {
            System.out.print(array[j] + " ");
        }
    }
}




