package task03_Arrays_Collections1;


/**
 * B. Видалити в масиві всі числа, які повторюються більше двох разів.
 */

public class ArrayExceptRepeated {

    public static void main(String[] args) {
        int[] array = {1, 5, 2, 5, 5, 4, 2, 6, 1, 4, 3, 9, 2, 14, 1, 4};
        int[] counted = counter(array);
        int sizeDestination = getArraySize(counted);
        int[] result = getDestinationArray(counted, array, sizeDestination);
        printArray(result);
    }

    public static int[] counter(int[] array) {
        int[] array1 = new int[array.length];
        int a = 0;
        int b = 0;
        for (int i = 0; i < array.length; i++) {
            a = array[i];
            for (int j = 0; j < array.length; j++) {
                if (a == array[j]) {
                    b++;
                }
            }
            array1[i] = b;
            b = 0;
        }
        return array1;
    }

    public static int getArraySize(int[] counter) {
        int count = 0;
        for (int i = 0; i < counter.length; i++) {
            if (counter[i] < 3) {
                count++;
            }
        }
        return count;
    }

    public static int[] getDestinationArray(int[] array1, int[] array2, int size) {
        int count1 = 0;
        int index = 0;
        int[] mergedArray = new int[size];

        for (int j = 0; j < array1.length; j++) {
            if (array1[j] < 3) {
                index = j;
                mergedArray[count1] = array2[index];
                count1++;
            }
        }
        return mergedArray;
    }

    public static void printArray(int[] array) {

        System.out.print("Array of elements, except of repeated elements more two times: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}




