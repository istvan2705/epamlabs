package task03_Arrays_Collections1;

/**
 * Знайти в масиві всі серії однакових елементів, які йдуть підряд,
 * і видалити з них всі елементи крім одного.
 */

public class FindSeriesElements {
    public static void main(String[] args) {

        int[] array = {3, 3, 5, 5, 8, 8, 8, 4, 7, 7, 5, 5, 9, 2, 2, 2, 2, 1, 1, 1};
        int size = getArraySize(array);
        int[] destinationArray = findSeriesOfSameElemets(array, size);
        printArray(destinationArray);
    }

    public static int getArraySize(int[] array) {
        int count = 1;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] != array[i + 1]) {
                count++;
            }
        }
        return count;
    }

    public static int[] findSeriesOfSameElemets(int[] array, int size) {

        int count2 = 0;
        int[] destArray = new int[size];
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] != array[i + 1]) {
                destArray[count2] = array[i];
                count2++;
            }
            destArray[count2] = array[i + 1];
        }
        return destArray;
    }

    public static void printArray(int[] array) {
        System.out.println("Series consecutive elements without repeated elements: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}

