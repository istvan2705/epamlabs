package task10_IO_NIO;

/**Write a program that displays the contents of a specific directory
 * (file and folder names + their attributes) with the possibility of setting
 * the current directory (similar to “dir” and “cd” command line commands).
 */

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ReadDirectory {
    public static File file = new File("/home/istvan/Documents/ENGLISH");

    public static void main(String[] args) throws IOException {
        System.out.println("Hello User");
        System.out.println("Please enter one of the listed commands:");
        System.out.println("1 - to print the current directory");
        System.out.println("2 - to print contents of specific directory");
        System.out.println("3 - to set current directory");
        readConsole();
    }

    private static void readConsole() throws IOException {
        while (true) {
            Scanner scan = new Scanner(System.in);
            String number = scan.nextLine();

            switch (number) {
                case "1":
                    String currentDirectory = getCurrentDirectory();
                    printDirectory(currentDirectory);
                    break;
                case "2":
                    printListOfFiles(file);
                    break;
                case "3":
                    String newDirectory = setCurrentDirectory();
                    printDirectory(newDirectory);
                    break;
                default:
                    System.out.println("Command is not correct. Please enter one of the numbers: 1, 2, 3");
                    break;
            }
        }
    }

    private static String setCurrentDirectory() {
        File file = new File(".");
        System.setProperty("user.dir", "Documents");
        String newDirectoryPath = file.getAbsolutePath();
        return newDirectoryPath;
    }

    private static String getCurrentDirectory() {
        String current = System.getProperty("user.dir");
        return current;
    }

    private static void printDirectory(String directory) {
        System.out.println(directory);
    }

    private static void printListOfFiles(File file) throws IOException {

        boolean fileExists = file.exists();
        boolean isDirectory = file.isDirectory();

        if (!fileExists) {
            System.out.println("Folder or file does not exists");
        }
        if (isDirectory) {
            File[] listOfFiles = file.listFiles();
            if (listOfFiles.length < 1)
                System.out.println("There is no files inside Folder");

            else System.out.println("List of Files and Folders");

            for (File file1 : listOfFiles) {
                if (!file1.isDirectory()) {
                    System.out.println(file1.getCanonicalPath());
                }
                if (file1.isDirectory()) {
                    System.out.println("There are next files in folder: " + file1);
                    String[] files = file1.list();
                    for (int i = 0; i < files.length; i++) {
                        String fileName = files[i];
                        System.out.println(fileName);
                    }
                }
            }
        }
    }
}







