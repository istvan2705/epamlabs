package task10_IO_NIO;

/**
 * Write a program that reads a Java source-code file (you provide the file name on the command line)
 * and displays all the comments. Do not use regular expression.
 */

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ReadComments {
    public static final String COMMENTS = "//";
    public static final String SLASH_STAR = "/*";
    public static final String STAR = "*";
    public static final String POINT_STAR = ".*";
    public static Path PATH1 = Paths.get("/home/istvan/IdeaProjects/EpamLabs/src/main/resources/Persons.java");
    public static Path PATH2 = Paths.get("/home/istvan/IdeaProjects/EpamLabs/src/main/resources/InputStreamTest.java");
    public static List<Path> paths = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        paths.add(PATH1);
        paths.add(PATH2);
        readConsole();
    }

    private static void readConsole() throws IOException {
        while (true) {
            System.out.println("Hello User");
            System.out.println("Please enter one of the listed java files to display only commented rows:" +
                    " Persons.java or InputStreamTest.java");
            Scanner scan = new Scanner(System.in);
            String fileName = scan.nextLine();
            if (fileName.equals("Persons.java") || (fileName.equals("InputStreamTest.java"))) {
                Path foundPath = getPath(paths, fileName);
                List<String> list = getList(foundPath);
                List<String> list1 = getComments(list);
                printList(list1);
            } else {
                System.out.println("The filename is incorrect. Please enter only listed");
            }
        }
    }

    private static Path getPath(List<Path> paths, String fileName) {
        Path foundPath = null;
        for (Path path : paths) {
            String file = path.getFileName().toString();
            if (file.equals(fileName)) {
                foundPath = path;
            }
        }
        return foundPath;
    }

    private static List<String> getList(Path path) throws FileNotFoundException {

        FileReader file = new FileReader(path.toString());
        BufferedReader reader = new BufferedReader(file);
        List<String> list = reader.lines().collect(Collectors.toList());
        return list;
    }

    private static List<String> getComments(List<String> list) {
        List<String> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).contains(COMMENTS)) {
                newList.add(list.get(i).substring(list.get(i).indexOf(COMMENTS)));
            }

            if (list.get(i).contains(SLASH_STAR) || list.get(i).contains(STAR) && !list.get(i).contains(POINT_STAR)) {
                newList.add(list.get(i));
            }
        }
        return newList;
    }

    private static void printList(List<String> list) {
        for (String row : list) {
            System.out.println(row);
        }
    }
}



