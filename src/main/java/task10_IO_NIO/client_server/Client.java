package task10_IO_NIO.client_server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

public class Client {


    public static void main(String[] args) throws IOException, InterruptedException{

        InetSocketAddress address = new InetSocketAddress("localhost", 4493);
        SocketChannel channel = SocketChannel.open(address);
        printMessage("Connecting to Server on port 4493...");

        ArrayList<String> footballTeams = new ArrayList<>();
        footballTeams.add("Juventus");
        footballTeams.add("Bayern");
        footballTeams.add("Barcelona");
        footballTeams.add("Liverpool");
        footballTeams.add("Ajax");


        for (String team : footballTeams) {
            ByteBuffer buffer = ByteBuffer.wrap(team.getBytes());
            channel.write(buffer);
            printMessage("sending: " + team);
            buffer.clear();
            Thread.sleep(2000);
        }
        channel.close();
    }

    private static void printMessage(String message) {
        System.out.println(message);
    }
}
