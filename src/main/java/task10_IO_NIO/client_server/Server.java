package task10_IO_NIO.client_server;


/**
 * Write client-server program using NIO (+ if you want, other implementation using IO).
 * E.g you have one server and multiple clients. A client can send direct messages to other client.
 */

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {

    public static int PORT = 4493;

    public static void main(String[] args) throws IOException {
        Selector selector = Selector.open();
        ServerSocketChannel serverSocket = ServerSocketChannel.open();
        serverSocket.bind(new InetSocketAddress("localhost", PORT));
        serverSocket.configureBlocking(false);
        serverSocket.register(selector, SelectionKey.OP_ACCEPT);
        printMessage("Server began listening on port: " + PORT);

        while (true) {
            {
                int num = selector.select();
                if (num == 0) {
                    continue;
                }
            }

            printMessage("I am a server and I'm waiting for new connection");
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectedKeys.iterator();
            while (iterator.hasNext()) {

                SelectionKey key = iterator.next();

                if (key.isAcceptable()) {
                    register(selector, serverSocket);

                } else if (key.isReadable()) {
                    SocketChannel client = (SocketChannel) key.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(256);
                    client.read(buffer);
                    String result = new String(buffer.array()).trim();
                    printMessage("Message received: " + result);
                    if (result.equals("Ajax")) {
                        client.close();
                    }
                }
                iterator.remove();
            }
        }
    }

    private static void register(Selector selector, ServerSocketChannel serverSocket) throws IOException {
        SocketChannel client = serverSocket.accept();
        client.configureBlocking(false);
        client.register(selector, SelectionKey.OP_READ);
        printMessage("Connection accepted: " + serverSocket.getLocalAddress());
    }

    private static void printMessage(String message) {
        System.out.println(message);
    }
}