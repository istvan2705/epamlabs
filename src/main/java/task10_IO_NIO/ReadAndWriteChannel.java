package task10_IO_NIO;

/**Create ReadAndWriteChannel class, which can be used for read and write data from/to channel"*/


import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Scanner;

public class ReadAndWriteChannel {

    public static void main(String[] args) throws IOException {
        System.out.println("Hello User");
        System.out.println("Please enter one of the listed commands:");
        System.out.println("1 - to read data from channel");
        System.out.println("2 - to write data to channel");
        readConsole();
    }

    private static void readConsole() throws IOException {
        RandomAccessFile file = new RandomAccessFile("/home/istvan/IdeaProjects/EpamLabs/src/main/resources/text1.txt", "r");
        RandomAccessFile file1 = new RandomAccessFile("/home/istvan/IdeaProjects/EpamLabs/src/main/resources/text2.txt", "rw");
        while (true) {
            Scanner scan = new Scanner(System.in);
            String number = scan.nextLine();

            switch (number) {
                case "1":
                    readFile(file);
                    break;
                case "2":
                    writeToFile(file1);
                    break;
                default:
                    System.out.println("Command is not correct. Please enter one of the numbers: 1, 2");
                    break;
            }
        }
    }

    private static void readFile(RandomAccessFile file) throws IOException {
        FileChannel inChannel = file.getChannel();
        ByteBuffer buf = ByteBuffer.allocate(48);
        int bytesRead = inChannel.read(buf);
        while (bytesRead != -1) {
            buf.flip();
            while (buf.hasRemaining()) {
                System.out.print((char) buf.get());
            }
            buf.clear();
            bytesRead = inChannel.read(buf);
        }
        file.close();
    }

    private static void writeToFile(RandomAccessFile file) throws IOException {
        FileChannel outChannel = file.getChannel();
        String text = "Hello World";
        ByteBuffer buf = ByteBuffer.allocate(48);
        buf.clear();
        buf.put(text.getBytes());
        buf.flip();
        while (buf.hasRemaining()) {
            outChannel.write(buf);
            System.out.println("Text has been written to file "+file);
        }
        outChannel.close();
    }
}

