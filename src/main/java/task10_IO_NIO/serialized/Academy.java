package task10_IO_NIO.serialized;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Academy implements Serializable {
    List<Person> personList = new ArrayList<>();

    public List<Person> addPerson(Person person) {
        personList.add(person);
        return personList;
    }

    public void showAll() {
        for (Person p : personList) {
            System.out.println(p.showData());
        }
    }
}
