package task10_IO_NIO.serialized;

import java.io.Serializable;

abstract class Person implements Serializable {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    abstract String showData();
}
