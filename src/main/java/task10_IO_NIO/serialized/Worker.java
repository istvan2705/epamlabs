package task10_IO_NIO.serialized;

import java.io.Serializable;

public class Worker extends Person implements Serializable {
    private String workPlace;

    public Worker(String name, String workplace) {
        super(name);
        this.workPlace = workplace;
    }

   public String getWorkPlace() {
        return workPlace;
    }

    @Override
    public String showData() {
        return String.format(" name: %s workplace: %s", getName(), getWorkPlace());
    }
}
