package task10_IO_NIO.serialized;

import java.io.Serializable;

public class Student extends Person implements Serializable {
    private String education;

    public Student(String name, String education) {
        super(name);
        this.education = education;
    }

    public String getEducation() {
        return education;
    }

    @Override
    public String showData() {
        return String.format(" name: %s education: %s", getName(), getEducation());
    }
}
