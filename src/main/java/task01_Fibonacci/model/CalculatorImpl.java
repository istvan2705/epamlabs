package task01_Fibonacci.model;

import java.util.LinkedList;
import java.util.List;

public class CalculatorImpl implements Calculator {

    @Override
    public String getOddNumbersOnInterval(int startInterval, int endInterval) {
        /**
         * This method is used to get odd numbers from start to the end of interval
         * @param startInterval This is the first parameter;
         * @param endInterval  This is the second parameter;
         * @return odd numbers of given interval;
         */
        StringBuilder oddNumbers = new StringBuilder();
        for (int i = startInterval; i <= endInterval; i++) {
            if (Math.abs(i % 2) == 1) {
                oddNumbers.append(i).append(", ");
            }
        }
        return oddNumbers.toString();
    }

    @Override
    public String getEvenNumbersOnInterval(int startInterval, int endInterval) {
        /** This method is used to get even numbers from end to start of interval
         *  @param startInterval This is the first parameter;
         * @param endInterval  This is the second parameter;
         * @return even numbers of given interval;
         */
        StringBuilder evenNumbers = new StringBuilder();
        for (int i = endInterval; i >= startInterval; i--) {
            if (Math.abs(i % 2) == 0) {
                evenNumbers.append(i).append(", ");
            }
        }
        return evenNumbers.toString();
    }

    @Override
    public int getSumOfOddNumbersOnInterval(int startInterval, int endInterval) {
        /**
         * This method is used to get sum of odd numbers from start to the end of interval
         * @param startInterval This is the first parameter;
         * @param endInterval  This is the second parameter;
         * @return sum odd numbers of given interval;
         */
        int sum = 0;
        for (int i = startInterval; i <= endInterval; i++) {
            if (Math.abs(i % 2) == 1) {
                sum += i;
            }
        }
        return sum;
    }

    @Override
    public int getSumOfEvenNumbersOnInterval(int startInterval, int endInterval) {
        /** This method is used to get sum of even numbers from end to start of interval
         * @param startInterval This is the first parameter;
         * @param endInterval  This is the second parameter;
         * @return sum even numbers of given interval;
         */
        int sum = 0;
        for (int i = endInterval; i >= startInterval; i--) {
            if (Math.abs(i % 2) == 0) {
                sum += i;
            }
        }
        return sum;
    }

    @Override
    public List<Integer> getFibonacciSequence(int biggestFibonacciNumber) {
        /**
         * This method is used to get List of Fibonacci numbers;
         * @param biggestFibonacciNumber This is the Fibonacci's biggest number;
         * @return Fibonacci sequence;
         */
        int previousNumber = 1;
        int currentNumber = 1;
        int nextNumber = 0;
        List<Integer> list = new LinkedList<>();
        while (nextNumber < biggestFibonacciNumber) {
            nextNumber = previousNumber + currentNumber;
            previousNumber = currentNumber;
            currentNumber = nextNumber;
            list.add(currentNumber);
        }
        return list;
    }

    @Override
    public int getAmountEvenNumbersOfFibonacci(List<Integer> listOfFibonacciNumbers) {
        /** This method is used to get sum of even numbers of Fibonacci sequence
         * @param listOfFibonacciNumbers This is the list of Fibonacci numbers;
         * @return sum even numbers of Fibonacci sequence;
         */
        int counterEvenNumbers = 0;
        for (Integer number : listOfFibonacciNumbers) {
            if (Math.abs(number % 2) == 0) {
                counterEvenNumbers++;
            }
        }
        return counterEvenNumbers++;
    }

    @Override
    public int getAmountOddNumbersOfFibonacci(List<Integer> listOfFibonacciNumbers) {
        /** This method is used to get sum of even numbers of Fibonacci sequence
         * @param listOfFibonacciNumbers This is the list of Fibonacci numbers;
         * @return sum odd numbers of Fibonacci sequence;
         */
        int counterOddNumbers = 2;
        for (Integer number : listOfFibonacciNumbers) {
            if (Math.abs(number % 2) == 1) {
                counterOddNumbers++;
            }
        }
        return counterOddNumbers++;
    }

    @Override
    public float getPercentageOddFibonacci(int amountOfOddNumbers, int amountOfEvenNumbers) {
        /** This method is used to get sum of even numbers of Fibonacci sequence
         * @param sumOfOddNumbers This is the sum of Odd Fibonacci numbers;
         * @param sumOfEvenNumbers This is the sum of Even Fibonacci numbers;
         * @return percentage of Odd numbers in Fibonacci sequence;
         */
        float percentageOdd = (amountOfOddNumbers * 100) / (amountOfOddNumbers + amountOfEvenNumbers);
        return percentageOdd;
    }

    @Override
    public float getPercentageEvenFibonacci(int amountOfOddNumbers, int amountOfEvenNumbers) {
        /** This method is used to get sum of even numbers of Fibonacci sequence
         * @param sumOfOddNumbers This is the sum of Odd Fibonacci numbers;
         * @param sumOfEvenNumbers This is the sum of Even Fibonacci numbers;
         * @return percentage of Even numbers in Fibonacci sequence;
         */
        float percentageEven = (amountOfEvenNumbers * 100) / (amountOfOddNumbers + amountOfEvenNumbers);
        return percentageEven;
    }
}
