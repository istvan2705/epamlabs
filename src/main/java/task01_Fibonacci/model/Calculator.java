package task01_Fibonacci.model;

import java.util.List;

public interface Calculator {

    int getSumOfOddNumbersOnInterval(int startInterval, int endInterval);

    int getSumOfEvenNumbersOnInterval(int startInterval, int endInterval);

    String getOddNumbersOnInterval(int startInterval, int endInterval);

    String getEvenNumbersOnInterval(int startInterval, int endInterval);

    List<Integer> getFibonacciSequence(int biggestFibonacciNumber);

    int getAmountEvenNumbersOfFibonacci(List<Integer> list);

    int getAmountOddNumbersOfFibonacci(List<Integer> list);

    float getPercentageOddFibonacci(int oddNumber, int evenNumber);

    float getPercentageEvenFibonacci(int oddNumber, int evenNumber);

}
