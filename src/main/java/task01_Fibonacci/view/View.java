package task01_Fibonacci.view;

public interface View{
     String EVEN_NUMBERS = "Even numbers on interval";
     String ODD_NUMBERS = "Odd numbers on interval";
     String SUM_OF_EVEN = "Sum of even numbers on interval";
     String SUM_OF_ODD = "Sum of even numbers on interval";
     String PERCENTAGE_OF_EVEN = "Percentage of even numbers in Fibonacci sequence";
     String PERCENTAGE_OF_ODD = "Percentage of odd numbers in Fibonacci sequence";
     String FIBONACCI_SEQUENCE = "Fibonacci sequence";

    int read();
}


