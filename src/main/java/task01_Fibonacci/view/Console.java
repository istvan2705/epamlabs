package task01_Fibonacci.view;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Console implements View {

    @Override
    public int read() {
        try {
            Scanner scanner = new Scanner(System.in);
            return scanner.nextInt();
        } catch (NoSuchElementException e) {
            System.out.println("You have entered not a number. Please enter only number");
            return read();
        }
    }
}




