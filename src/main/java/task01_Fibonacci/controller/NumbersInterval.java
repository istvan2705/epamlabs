package task01_Fibonacci.controller;

import task01_Fibonacci.model.Calculator;

public class NumbersInterval {
    private int startInterval;
    private int endInterval;
    private Calculator calculator;

    public NumbersInterval(int startInterval, int endInterval, Calculator calculator) {
        this.startInterval = startInterval;
        this.endInterval = endInterval;
        this.calculator = calculator;
    }

    public String getEvenNumbersOnInterval() {
        return calculator.getEvenNumbersOnInterval(startInterval, endInterval);
    }

    public String getOddNumbersOnInterval() {
        return calculator.getOddNumbersOnInterval(startInterval, endInterval);
    }

    public int getSumOfEvenNumberOnInterval() {
        return calculator.getSumOfEvenNumbersOnInterval(startInterval, endInterval);
    }

    public int getSumOfOddNumberOnInterval() {
        return calculator.getSumOfOddNumbersOnInterval(startInterval, endInterval);
    }
}

