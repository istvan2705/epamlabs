package task01_Fibonacci.controller;

import task01_Fibonacci.model.Calculator;
import task01_Fibonacci.model.CalculatorImpl;
import task01_Fibonacci.view.Console;
import task01_Fibonacci.view.View;

import java.util.HashMap;
import java.util.LinkedHashMap;

import static task01_Fibonacci.view.View.*;

public class MainController {

    View view = new Console();
    Calculator calculator = new CalculatorImpl();

    public MainController() {
    }

    public void run() {
        while (true) {
            System.out.println("Please enter the start number of interval:");
            int startInterval = view.read();
            System.out.println("Please enter the end number of interval:");
            int endInterval = view.read();
            System.out.println("Please enter the Fibonacci size");
            int biggestFibonacciNumber = view.read();

            NumbersInterval interval = new NumbersInterval(startInterval, endInterval, calculator);
            FibonacciSequence fibonacciSequence = new FibonacciSequence(biggestFibonacciNumber, calculator);
            HashMap<Object, Object> result = new LinkedHashMap<>();
            result.put(EVEN_NUMBERS, interval.getEvenNumbersOnInterval());
            result.put(ODD_NUMBERS, interval.getOddNumbersOnInterval());
            result.put(SUM_OF_EVEN, interval.getSumOfEvenNumberOnInterval());
            result.put(SUM_OF_ODD, interval.getSumOfOddNumberOnInterval());
            result.put(PERCENTAGE_OF_EVEN, fibonacciSequence.getPercentageOfEvenFibonacci());
            result.put(PERCENTAGE_OF_ODD, fibonacciSequence.getPercentageOfOddFibonacci());
            result.put(FIBONACCI_SEQUENCE, fibonacciSequence.getFibonacciSequence());
            result.forEach((k, v) -> System.out.println(k + ":" + v));
        }
    }
}
