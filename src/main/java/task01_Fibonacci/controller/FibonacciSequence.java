package task01_Fibonacci.controller;

/**
 * <h1>Fibonacci</h1>
 * User enter the interval (for example: [1;100]);
 * Program prints odd numbers from start to the end of interval and even from end to start;
 * Program prints the sum of odd and even numbers;
 * Program build Fibonacci numbers: F1 will be the biggest odd
 * number and F2 – the biggest even number, user can enter the size
 * of set (N);
 * Program prints percentage of odd and even Fibonacci numbers;
 * <p>
 *
 * @author Stepan Kish
 * @version 1.0
 * @since 2019-01-22
 */

import task01_Fibonacci.model.Calculator;

import java.util.List;

public class FibonacciSequence {
    private Calculator calculator;
    private int fibonacciSize;

    public FibonacciSequence(int fibonacciSize, Calculator calculator) {
        this.fibonacciSize = fibonacciSize;
        this.calculator = calculator;
    }

    public List<Integer> getFibonacciSequence() {
        return calculator.getFibonacciSequence(fibonacciSize);
    }

    public int getAmountEvenNumbersOfFibonacci() {
        return calculator.getAmountEvenNumbersOfFibonacci(getFibonacciSequence());
    }

    public int getAmountOfOddNumbersOfFibonacci() {
        return calculator.getAmountOddNumbersOfFibonacci(getFibonacciSequence());
    }

    public float getPercentageOfEvenFibonacci() {
        return calculator.getPercentageEvenFibonacci(getAmountOfOddNumbersOfFibonacci(),
                getAmountEvenNumbersOfFibonacci());
    }

    public float getPercentageOfOddFibonacci() {
        return calculator.getPercentageOddFibonacci(getAmountOfOddNumbersOfFibonacci(),
                getAmountEvenNumbersOfFibonacci());
    }
}
