package task14_JSON;

import com.fasterxml.jackson.databind.ObjectMapper;
import task13_XML.Bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JacksonJSONParser {
public static final String PATH_JSON = "/home/istvan/IdeaProjects/EpamLabs/src/main/resources.banks.txt";
    public static void main(String[] args) throws IOException {

        byte[] jsonData = Files.readAllBytes(Paths.get(PATH_JSON));
          ObjectMapper objectMapper = new ObjectMapper();
          Bank bank = objectMapper.readValue(jsonData, Bank.class);
        System.out.println("Bank Object\n"+ bank);
    }
}
